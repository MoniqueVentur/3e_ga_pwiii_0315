import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcompanhamentoDePedidosComponent } from './acompanhamento-de-pedidos.component';

describe('AcompanhamentoDePedidosComponent', () => {
  let component: AcompanhamentoDePedidosComponent;
  let fixture: ComponentFixture<AcompanhamentoDePedidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcompanhamentoDePedidosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcompanhamentoDePedidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
