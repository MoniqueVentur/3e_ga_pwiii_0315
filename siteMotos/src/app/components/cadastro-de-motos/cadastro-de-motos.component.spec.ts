import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroDeMotosComponent } from './cadastro-de-motos.component';

describe('CadastroDeMotosComponent', () => {
  let component: CadastroDeMotosComponent;
  let fixture: ComponentFixture<CadastroDeMotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadastroDeMotosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroDeMotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
