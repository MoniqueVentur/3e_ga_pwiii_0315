import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsComponent } from './components/components.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { CadastroDeClientesComponent } from './components/cadastro-de-clientes/cadastro-de-clientes.component';
import { TrailComponent } from './components/trail/trail.component';
import { StreetComponent } from './components/street/street.component';
import { CustomComponent } from './components/custom/custom.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { SportComponent } from './components/sport/sport.component';
import { TouringComponent } from './components/touring/touring.component';
import { CadastroDeUsuariosComponent } from './components/cadastro-de-usuarios/cadastro-de-usuarios.component';
import { CadastroDeMotosComponent } from './components/cadastro-de-motos/cadastro-de-motos.component';
import { AcompanhamentoDePedidosComponent } from './components/acompanhamento-de-pedidos/acompanhamento-de-pedidos.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponentsComponent,
    HomeComponent,
    LoginComponent,
    CadastroDeClientesComponent,
    TrailComponent,
    StreetComponent,
    CustomComponent,
    ScooterComponent,
    SportComponent,
    TouringComponent,
    CadastroDeUsuariosComponent,
    CadastroDeMotosComponent,
    AcompanhamentoDePedidosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
